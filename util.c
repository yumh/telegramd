#include "openbsd-compat/compat.h"

#include <string.h>
#include <stdlib.h>

#include "util.h"

/* replace *first* occurence of string `patt' with `sub' in `src' */
char *
strrf(const char *src, const char *patt, const char *sub)
{
	char *c = strstr(src, patt);
	if (c == NULL)
		return strdup(src);

	/* ok - patt is within src */
	size_t srclen  = strlen(src);
	size_t pattlen = strlen(patt);
	size_t sublen  = strlen(sub);
	size_t reslen  = srclen - pattlen + sublen + 1;

	char *res = calloc(1, reslen);

	memcpy(res, src, c - src);
	strlcat(res, sub, reslen);
	strlcat(res, c + pattlen, reslen);

	return res;
}

/*
 * replace EVERY occurrence of patt with sub in src. The returned
 * string have to be free'd by the callee.
 */
char *
strrepl(const char *src, const char *patt, const char *sub)
{
	if (src  == NULL) return NULL;
	if (patt == NULL) return NULL;
	if (sub  == NULL) return NULL;

	char *r = strdup(src);
	while (strstr(r, patt) != NULL) {
		char *rr = strrf(r, patt, sub);
		free(r);
		r = rr;
	}

	return r;
}
