#ifndef SERVER_H
#define SERVER_H

#define SERVER_BACKLOG 32
#define SERVER_POLLFD_SIZE 256
#define SERVER_BUFLEN 256

#include "openbsd-compat/compat.h"

int	server_main(struct imsgbuf*);

#endif
