#ifndef CONFIG_H
#define CONFIG_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

enum dbtype {INVALID, SQLITE, MEM};

struct config {
	bool	 valid;
	char	*socket_path;

	struct {
		int	 port;
		char	*host;
	} server;

	struct {
		enum dbtype	 type;
		char		*file;
	} db;
};

bool		 parse_config(void);
struct config	 getconf(void);

#endif
