#include <stdio.h>
#include <string.h>

#include "compat.h"
#include "protocol.h"

char *
serialize_msg(struct command cmd)
{
	char	*ret;

	switch (cmd.type) {
	case MSG_FROM:
		asprintf(&ret, "message from %s\n", cmd.d.from);
		break;
	case MSG_TO:
		asprintf(&ret, "message to %s\n", cmd.d.to);
		break;
	case DATA:
		asprintf(&ret, "data\n%s\n", cmd.d.data.body);
		break;
	case RETRIEVE:
		asprintf(&ret, "");
		break;
	case DONE:
		asprintf(&ret, "done\n");
		break;
	default:
		asprintf(&ret, "invalid\n");
	}

	return ret;
}

/* INCOMPLETE! DO NOT USE! */
struct command
deserialize_msg(const char *m)
{
	struct command	c;
	char		*p;

	c.type = INVALID;

	if (m == NULL)
		return c;

	/* As of now, we can simply check the first character */
	switch (*m) {
	case 'm':
	case 'M': {
		if ((p = strcasestr(m, "message from ")) == m) {
			p += strlen("message from ");
			c.type = MSG_FROM;
			c.d.from = strdup(p);
		}

		if ((p = strcasestr(m, "message to ")) == m) {
			p += strlen("message to ");
			c.type = MSG_TO;
			c.d.to = strdup(p);
		}

		break;
	}
	case 'd':
	case 'D': {
		if ((p = strcasestr(m, "data ")) == m) {
			/* TODO: parse data option */
			p = strchr(m, '\n');
			if (p == NULL)
				break;
			p++;

			c.type = DATA;
			c.d.data.body = strdup(p);
		}

		if ((p = strcasestr(m, "done")) == m) {
			/* ... */
		}

		break;
	}
	}

	return c;
}
