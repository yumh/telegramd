#ifndef LOG_H
#define LOG_H

#include "openbsd-compat/compat.h"

#include <sys/cdefs.h>

#include <stdio.h>
#include <stdarg.h>

enum logprio { DEBUG, INFO, WARN, ERR };

/* The functions log_set* ARE NOT threadsafe, the other are */

/* change the file where the log will be written, default to 2 (stderr) */
void		log_setout(FILE*);

/* set the minimum priority for logs to show. Default to WARN */
void		log_setprio(enum logprio);

/* set an (optional) prefix  */
void		log_setprefix(const char*);

/* log a message with the given priority */
void		vlog(enum logprio, const char*, va_list);
void		llog(enum logprio, const char*, ...);

/* like openbsd's err, log the given message as ERR and then exit with the given code */
__dead void	err(int, const char*, ...);

#endif
