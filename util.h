#ifndef UTIL_H
#define UTIL_H

enum imsg_type { QUIT, FROM_FETCHER, FROM_SERVER };

char	*strrf(const char*, const char*, const char*);
char	*strrepl(const char*, const char*, const char*);

#endif
