#ifndef DB_H
#define DB_H

#include <sqlite3.h>

#define DB_MIGRATIONS_VERSION 1

sqlite3		*preparedb(void);

#endif
