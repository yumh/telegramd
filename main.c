#include <sys/socket.h>

#include <errno.h>
#include <poll.h>
#include <string.h>
#include <unistd.h>

#include "openbsd-compat/compat.h"

#include "config.h"
#include "server.h"
#include "fetcher.h"
#include "db.h"
#include "log.h"
#include "util.h"

int
main(void)
{
	struct imsgbuf to_fetcher, from_fetcher, to_server, from_server, ibuf;
	struct imsg imsg;
	sqlite3 *db;
	struct pollfd fds[2];
	ssize_t n, datalen;
	char *buf;

	int fetcher_fds[2], server_fds[2], i;

	/* log EVERYTHING */
	log_setout(stderr);
	log_setprio(DEBUG);

	llog(INFO, "Hello world, telegramd is starting");

	if (!parse_config())
		err(1, "Invalid config");

	/* prepare socket from/to fetcher */
	if (socketpair(AF_UNIX, SOCK_STREAM, PF_UNSPEC, fetcher_fds))
		err(1, NULL);

	switch (fork()) {
	case -1:
		err(1, NULL);
	case 0:
		/* fetcher */
		close(fetcher_fds[0]);
		imsg_init(&to_fetcher, fetcher_fds[1]);
		log_setprefix("fetcher");
		exit(fetcher_main(&to_fetcher));
	}
	close(fetcher_fds[1]);
	imsg_init(&from_fetcher, fetcher_fds[0]);

	if (socketpair(AF_UNIX, SOCK_STREAM, PF_UNSPEC, server_fds))
		err(1, NULL);

	switch (fork()) {
	case -1:
		err(1, NULL);
	case 0:
		/* server */
		close(server_fds[0]);
		imsg_init(&to_server, server_fds[1]);
		log_setprefix("server");
		exit(server_main(&to_server));
	}
	close(server_fds[1]);
	imsg_init(&from_server, server_fds[0]);

	log_setprefix("main");

	/* prepare db */
	db = preparedb();
	if (db == NULL)
		err(1, "preparedb() failed");

	fds[0].fd = fetcher_fds[0];
	fds[0].events = POLLIN;
	fds[0].revents = 0;

	fds[1].fd = server_fds[0];
	fds[1].events = POLLIN;
	fds[1].revents = 0;

	while (true) {
		if (poll(fds, 2, INFTIM) == -1)
			err(1, NULL);

		for (i = 0; i < 2; ++i) {
			if (fds[i].revents == 0)
				continue;

			if (fds[i].revents != POLLIN)
				err(1, "unknown revent %d", fds[i].revents);

			ibuf = i == 0 ? from_fetcher : from_server;

			/* get the message */
			if ((n = imsg_read(&ibuf)) == -1 && errno != EAGAIN)
				err(1, "imsg read error");
			if (n == 0)
				err(1, "imsg: other side closed");

			while (true) {
				if ((n = imsg_get(&ibuf, &imsg)) == -1)
					err(1, "imsg_get read error");
				if (n == 0)
					break;

				datalen = imsg.hdr.len - IMSG_HEADER_SIZE;
				buf = calloc(datalen + 1, sizeof(char));
				if (buf == NULL)
					err(1, "cannot allocate memory");
				memcpy(buf, imsg.data, datalen);
				llog(DEBUG, "got from %s: %s", imsg.hdr.type == FROM_FETCHER ? "fetcher" : "server", buf);

				/* dummy! */
				if (imsg.hdr.type == FROM_FETCHER) {
					imsg_compose(&from_server, FROM_FETCHER, 0, 0, -1, buf, datalen + 1);
					datalen = msgbuf_write(&from_server.w);
					if (datalen == -1 && errno != EAGAIN)
						err(1, "send() to buffer failed");
					if (datalen == 0)
						err(1, "other side closed");
				} else {
					
				}
				free(buf);
				imsg_free(&imsg);
			}
		}
	}
}
