%{
#include <stdio.h>
#include <err.h>
#include <string.h>

#include "config.h"
#include "util.h"

void		 set_field_num(char*, int);
void		 set_field_id(char*,  char*);
void		 set_field_str(char*, char*);

extern int	 yylex();
extern int	 yyparse();
extern void	 yyerror(char*);
extern FILE	*yyin;

struct config	 config = {
	.valid = true,
	.socket_path = NULL,
	.server = {
		.port = 1337,
		.host = "localhost"
	},
	.db = {
		.type = SQLITE,
		.file = "~/.config/telegramd/db"
	}
};
%}

%token NUMBER IDENTIFIER STRING EQ NEWL

%union {
	int number;
	char *string;
}

%token <number> NUMBER
%token <string> IDENTIFIER
%token <string> STRING

%%

config		: config def NEWL	{}
		| config NEWL		{}
		|			{}
		;

def		: IDENTIFIER EQ NUMBER		{set_field_num($1, $3);}
		| IDENTIFIER EQ IDENTIFIER	{set_field_id($1, $3); free($3);}
		| IDENTIFIER EQ STRING		{set_field_str($1, $3);}
		;

%%

bool
parse_config(void)
{
	char *file;
	asprintf(&file, "%s/.config/telegramd/telegramd.conf", getenv("HOME"));

	FILE *f = fopen(file, "r");
	free(file);
	if (f == NULL) {
		return false;
	}

	yyin = f;
	do {
		yyparse();
	} while (!feof(yyin));

	bool res = true;
	if (config.server.port == -1) {
		fprintf(stderr, "server.port invalid or not defined\n");
		res = false;
	}
	if (config.server.host == NULL) {
		fprintf(stderr, "server.host invalid or not defined\n");
		res = false;
	}
	if (config.db.type == INVALID) {
		fprintf(stderr, "db.type invalid or not defined\n");
		res = false;
	}
	if (config.db.file == NULL) {
		fprintf(stderr, "db.file invalid or not defined\n");
		res = false;
	}

	return res;
}

int
yywrap()
{
	return 1;
}

struct config
getconf(void)
{
	return config;
}

char *
homeify_path(char *s)
{
	if (s != NULL && *s == '~')
		return strrf(s, "~", getenv("HOME"));
	/* for consistency, always return a newly allocated string. */
	return strdup(s);
}

void
set_field_num(char *p, int v)
{
	if (!strcmp(p, "server.port")) {
		if (v > 0)
			config.server.port = v;
		else
			fprintf(stderr, "Cannot bind negative port.\n");
		return;
	}

	fprintf(stderr, "Wrong value (%d) or config '%s'\n", v, p);
}

void
set_field_id(char *p, char *v)
{
	if (!strcmp(p, "db.type")) {
		if (!strcmp(v, "sqlite"))
			config.db.type = SQLITE;
		else if (!strcmp(v, "mem")) {
			warn("Only SQLITE is implemented now, using that instead of MEM");
			config.db.type = SQLITE;
		} else
			fprintf(stderr, "Wrong value '%s' for db.type.", v);
		return;
	}

	config.db.type = INVALID;
	fprintf(stderr, "Wrong value (%s) or config '%s'\n", v, p);
}

void
set_field_str(char *p, char *v)
{
	char *s = strdup(v + 1); /* skip the opening quote */
	free(v);
	if (s == NULL) {
		warn("Error in memory allocation. Cannot set %s config", p);
		return;
	}
	size_t len = strlen(s);
	s[len-1] = '\0'; 	/* and skip the last quote */

	if (!strcmp(p, "server.host"))
		config.server.host = s;
	else if (!strcmp(p, "db.file")) {
		config.db.file = homeify_path(s);
		free(s);
	} else if (!strcmp(p, "server.socket_path")) {
		config.socket_path = homeify_path(s);
		free(s);
	} else {
		fprintf(stderr, "Wrong value (%s) or config '%s'\n", s, p);
		free(s);
	}
}
