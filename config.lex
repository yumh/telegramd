%{
#include <stdlib.h>  
#include <errno.h>
#include <limits.h>
#include <string.h>

#include "config.tab.h"

int chars = 1;
%}

%option lex-compat

%%

[0-9]+			{
				errno = 0;
				char *ep;
				long lval = strtol(yytext, &ep, 0);
				if (yytext[0] == '\0' || *ep != '\0')
					/* not a number */
					;
				if ((errno = ERANGE && (lval == LONG_MAX || lval == LONG_MIN))
					|| (lval > INT_MAX || lval < INT_MIN))
					/* out of range */
					;
				/* printf("found number %ld\n", lval); */
				yylval.number = lval;
				chars += strlen(yytext);
				return NUMBER;
			}

[ \t]			{ chars++; }
[\n]			{ chars =  1; return NEWL; }
[=]			{ chars += 1; return EQ; }
"#".*			{ /* ignore comments */ }

[-a-zA-Z._]+		{
				yylval.string = strdup(yytext);
				if (yylval.string == NULL)
					/* ups, memory allocation error! */
					;
				/* printf("found identifier %s\n", yytext); */
				chars += strlen(yytext);
				return IDENTIFIER;

			}

\"(\\.|[^"])*\"		{
				yylval.string = strdup(yytext);
				if (yylval.string == NULL)
					/* ups, memory allocation error! */
					;
				/* printf("found string %s\n", yytext); */
				chars += strlen(yytext);
				return STRING;
			}

.			{ ECHO; chars++; }

%%

void
yyerror(char *s)
{
	fprintf(stderr, "%s (at: %d:%d)\n", s, yylineno, chars);
	exit(1);
}
