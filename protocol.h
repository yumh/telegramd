#ifndef PROTOCOL_H
#define PROTOCOL_H

enum command_type {
	MSG_FROM,
	MSG_TO,
	DATA,
	RETRIEVE,
	DONE,
	INVALID
};

struct cmd_data {
	/* TODO: attributes (like Markdown, ...) */
	char	*body;
};

struct command {
	enum command_type type;
	union {
		char *from;
		char *to;
		struct cmd_data data;
	} d;
};

char		*serialize_msg(struct command);

/* INCOMPLETE FUNCTION. DO NOT USE! */
struct command	 deserialize_msg(const char*);

#endif
