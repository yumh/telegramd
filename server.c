#include "compat.h"

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <netinet/in.h>
#include <unistd.h>

#include "server.h"
#include "config.h"
#include "log.h"
#include "util.h"

static int
setup_socket(struct config c)
{
	int	s;
	int	one = 1;

	if (c.socket_path != NULL) {
		if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
			err(1, NULL);
	} else {
		if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
			err(1, NULL);
	}

	/* allow socket descriptor to be reusable */
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0)
		err(1, NULL);

	/* set socket to be non blocking */
	if (ioctl(s, FIONBIO, &one) < 0)
		err(1, NULL);

	/* prepare the struct sockaddr */
	if (c.socket_path != NULL) {
		struct		sockaddr_un addr;

		unlink(c.socket_path);

		addr.sun_family = AF_UNIX;
		strlcpy(addr.sun_path, c.socket_path, sizeof(addr.sun_path));

		if (bind(s, (struct sockaddr*)&addr, SUN_LEN(&addr)) < 0)
			err(1, NULL);
	} else {
		struct		sockaddr_in addr;

		addr.sin_family = AF_INET;

		/* TODO: use c.server.host */
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
		addr.sin_port = htons(c.server.port);

		if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0)
			err(1, NULL);
	}

	if (listen(s, SERVER_BACKLOG) < 0)
		err(1, NULL);

	return s;
}

static int
handle_client(int fd)
{
	int	t;
	char	buf[SERVER_BUFLEN] = {0};

	while (1) {
		t = recv(fd, buf, sizeof(buf), 0);

		if (t < 0) {
			if (errno == EAGAIN) {
				return 0;
			}
			return -1;
		}

		if (t == 0)
			return -1;

		/* tempted to put MSG_DONTWAIT here, but still */
		t = send(fd, buf, t, 0);
		if (t < 0) {
			llog(INFO, NULL);
			return -1;
		}
	}

	return 0;
}

static int
handle_new_clients(int s, struct pollfd *fds, int nfds)
{
	int	new_s;

	do {
		new_s = accept(s, NULL, NULL);
		if (new_s < 0)
			return nfds;

		if (nfds == SERVER_POLLFD_SIZE) {
			close(new_s);
			continue;
		}

		llog(INFO, "new client connected");
		fds[nfds].fd = new_s;
		fds[nfds].events = POLLIN;
		nfds++;
	} while (1);

	return nfds;
}

static int
compress_fds(struct pollfd *fds, int nfds)
{
	int	i, j;

	for (i = 0; i < nfds; i++)
		if (fds[i].fd == -1) {
			for (j = i; j < nfds -1; j++)
				fds[j].fd = fds[j+1].fd;
			nfds--;
		}

	return nfds;
}

static void
handle_imsg(struct imsgbuf *ibuf)
{
	struct imsg	imsg;
	ssize_t		n, datalen;

	if ((n = imsg_read(ibuf)) == -1 && errno != EAGAIN)
		err(1, "Write error");
	if (n == 0)
		err(1, "closed connection");

	for (;;) {
		if ((n = imsg_get(ibuf, &imsg)) == -1)
			err(1, "write error");
		if (n == 0) 	/* no more messages */
			return;
		datalen = imsg.hdr.len - IMSG_HEADER_SIZE;

		llog(DEBUG, "got: %s", imsg.data);
		imsg_free(&imsg);
	}
}

int
server_main(struct imsgbuf *ibuf)
{
	struct pollfd	fds[SERVER_POLLFD_SIZE];
	int		s;
	int		i;
	int		t;
	int		nfds;
	int		loop;

	s = setup_socket(getconf());

	explicit_bzero(fds, sizeof(fds));

	fds[0].fd = s;
	fds[0].events = POLLIN;

	fds[1].fd = ibuf->fd;
	fds[1].events = POLLIN;
	nfds = 2;

	llog(DEBUG, "Waiting for clients");

	loop = 1;
	while (loop) {
		int	current_size = nfds;

		if ((t = poll(fds, nfds, INFTIM)) == -1)
			err(1, NULL);

		for (i = 0; i < current_size; i++) {
			int	disconnected = 0;

			if (fds[i].revents == 0)
				continue;

			if (fds[i].revents & POLLHUP) {
				if (i == 1) {
					llog(DEBUG, "Main process exited, I'm exiting too.");
					loop = 0;
					break;
				} else
					disconnected = 1;

			} else if (fds[i].revents & POLLIN) {
				if (i == 0) /* new clients */
					nfds = handle_new_clients(s, fds, nfds);
				else if (i == 1) /* messages! */
					handle_imsg(ibuf);
				else
					disconnected = handle_client(fds[i].fd) == -1;
			}

			if (disconnected) {
				llog(DEBUG, "client disconnected.");
				close(fds[i].fd);
				fds[i].fd = -1;
				nfds = compress_fds(fds, nfds);
			}
		}
	}

	for (i = 0; i < nfds; ++i)
		if (fds[i].fd != -1)
			close(fds[i].fd);

	return 0;
}
