#include "openbsd-compat/compat.h"

#include <stdlib.h>

#include <sqlite3.h>

#include "db.h"
#include "config.h"
#include "log.h"

static int
pragma_callback(void *v, int argc, char **argv, char **colname)
{
	int *version = v;
	if (argc != 1)
		return -1;

	const char *errstr = NULL;
	*version = strtonum(*argv, 0, DB_MIGRATIONS_VERSION, &errstr);
	if (errstr != NULL)
		return -1;

	return 0;
}

/* Create the accounts nad contacts tables */
static const char *
migration_v1()
{
	return
		"BEGIN TRANSACTION;"
		" create table contacts ("
		"   id integer primary key,"
		"   fname text,"
		"   lname text,"
		"   nickname text"
		" );"
		" "
		" create table accounts ("
		"   id integer primary key references contacts(id),"
		"   token text not null"
		" );"
		" "
		" pragma user_version = 1;"
		" COMMIT;"
		;
}

sqlite3 *
preparedb(void)
{
	struct config c = getconf();

	sqlite3 *db;
	int rc = sqlite3_open(c.db.file, &db);
	if (rc) {
		llog(ERR, "cannot open db");
		return NULL;
	}

	char *ex = NULL;
	int user_version = -1;
	rc = sqlite3_exec(db, "pragma user_version;", &pragma_callback, &user_version, &ex);
	if (rc != SQLITE_OK) {
		llog(ERR, "unable to read pragma user_version or incorrect value: %s", ex);
		sqlite3_free(ex);
		goto err;
	}

	llog(DEBUG, "DB version %d", user_version);

	if (user_version < 1) {
		llog(INFO, "running migratio #1");
		rc = sqlite3_exec(db, migration_v1(), NULL, NULL, &ex);

		if (rc != SQLITE_OK) {
			llog(ERR, "Up for migration failed: %s", ex);
			sqlite3_free(ex);
			goto err;
		}

		user_version = 1;
	}

	return db;

 err:	sqlite3_close(db);
	return NULL;
}
