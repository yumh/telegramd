#ifndef COMPAT_H
#define COMPAT_H

#include <poll.h>
#ifndef INFTIM
# define INFTIM -1
#endif

#include <stdlib.h>

#ifndef HAVE_IMSG
# include <sys/socket.h>
# include <stdint.h>
# include "queue.h"
# include "imsg.h"
#else
# include <sys/types.h>
# include <sys/queue.h>
# include <sys/uio.h>
# include <stdint.h>
# include <imsg.h>
#endif

#ifndef HAVE_REALLOCARRAY
void *reallocarray();
#endif

#ifndef HAVE_RECALLOCARRAY
void *recallocarray(void*,size_t,size_t,size_t);
#endif

#ifndef HAVE_EXPLICIT_BZERO
void explicit_bzero(void*,size_t);
#endif

#ifndef HAVE_STRTONUM
long long strtonum(const char*, long long, long long, const char**);
#endif

#ifndef HAVE_FREEZERO
void freezero(void*, size_t);
#endif

#ifndef HAVE_STRLCAT
size_t strlcat(char*, const char*, size_t);
#endif

#ifndef HAVE_STRLCPY
size_t strlcpy(char*, const char*, size_t);
#endif

#ifndef HAVE_GETDTABLECOUNT
#define getdtablecount()	0
#endif

#ifndef __dead
#define __dead
#endif

#ifndef IOV_MAX
# if defined(_XOPEN_IOV_MAX)
#  define       IOV_MAX         _XOPEN_IOV_MAX
# elif defined(DEF_IOV_MAX)
#  define       IOV_MAX         DEF_IOV_MAX
# else
#  define       IOV_MAX         16
# endif
#endif

#endif
