#include <stdbool.h>
#include <unistd.h>
#include <errno.h>

#include "fetcher.h"
#include "log.h"
#include "util.h"

int
fetcher_main(struct imsgbuf *ibuf)
{
	log_setprefix("fetcher");

	while (true) {
		char buf[] = "HELLOOO";
		int len = sizeof(buf);
		imsg_compose(ibuf, FROM_FETCHER, 0, 0, -1, &buf, len);
		len = msgbuf_write(&ibuf->w);
		if (len == -1 && errno != EAGAIN)
			err(1, "send() to buffer failed");

		if (len == 0)
			err(1, "other side closed");
		sleep(5);
	}
}
