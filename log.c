#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "log.h"

enum logprio	 priority = WARN;
FILE		*out = NULL;
const char	*prefix = NULL;

static const char *
log_getprgname()
{
#if HAVE_GETPROGNAME
	return getprogname();
#else
	return "telegramd";
#endif
}

void
log_setout(FILE *f)
{
	out = f;
}

void
log_setprio(enum logprio p)
{
	priority = p;
}

void
log_setprefix(const char *p)
{
	prefix = p;
}

void
vlog(enum logprio p, const char *fmt, va_list pa)
{
	if (p < priority)
		return;
	fprintf(out, "%s: ", log_getprgname());
	if (prefix != NULL)
		fprintf(out, "%s: ", prefix);
	if (fmt == NULL)
		fprintf(out, "%s", strerror(errno));
	else
		vfprintf(out, fmt, pa);
	fputc('\n', out);
}

void
llog(enum logprio p, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vlog(p, fmt, args);
	va_end(args);
}

void
err(int ex, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vlog(ERR, fmt, args);
	va_end(args);
	exit(ex);
}
